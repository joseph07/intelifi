usage() {
echo ""
echo "This is a convenience script for automating variant filtering using the default AdaBoost model. "
echo "It takes as inputs the VCF and ARFF file produced by multiSNV and computes all features required by the default AdaBoost model. In particular it:"
echo ""
echo "3) Runs flagPossiblyGermline.py"
echo "4) Runs flagAsymmetricMapping.py"
echo "5) Runs flagNearbyIndels.py"
echo "6) Runs flagOverlappingEntries.py with a) dbSNP version 137 excluding sites 129 (in datasets/) b) PoN (in datasets/) c) DukeBL( in datasets/) c) DacBL (in datasets/) d)LCR (in datasets/)  "
echo ""
echo "The PoN was created at the CRUK-CI by running MuTect in artefact detection mode using 112 normal samples. The DukeBL, DacBL and LCR data are blacklist regions. We describe where these come from in the README file. "
echo "The output is a VCF file with only SNVs predicted as true (${fin}_AdaBoost.vcf) and an arff file with all input features ${fin}_allFlags.arff."
echo ""
echo "Usage:"
echo ""
echo "$0 --bams=\"bam1.bam bam2.bam\" --fin=/PATH/TO/input.vcf --arff=/PATH/TO/arff.vcf --model=/PATH/TO/MODEL/somemodel.pkl [-h |--help] "

echo ""
echo "Options:"
echo ""
echo "--bams   Input BAM files in the same order that was used to run multiSNV i.e \"bam1.bam bam2.bam bam3.bam\""
echo "--model  Trained model to use for prediction. We recommend using the default model default/model/default_model.pkl";
#echo "--validated = Optional: Pass any experimentally validated SNVs in a tab-delimited file of CHROM \t POSITION ";
echo "--fin    VCF produced by multiSNV"
echo "--arff   ARFF produced by multiSNV using --features option"
echo "--help   Produce this help message"
echo "";
exit
}

if [ $# -eq 0 ]; then
usage
exit 1;
fi

while [ "$#" -gt 0 ]; do
case "$1" in
--help) usage; exit 1;;
--bams=*) BAMS="${1#*=}"; shift 1;;
--model=*) MODEL="${1#*=}"; shift 1;;
--fin=*) multisnvVCF="${1#*=}"; shift 1;;
--arff=*) multisnvARFF="${1#*=}"; shift 1;;
--validated=*) VALIDATED="${1#*=}"; shift 1;;
--bams|--model|--fin|--arff) echo "$1 requires an argument" >&2; exit 1;;
-*) echo "unknown option: $1" >&2; exit 1;;
*) die "unrecognized argument: $1";;
esac
done

if [ -z "$BAMS" ] ; then
echo "No BAMS specified"
exit 1;
fi

#if [ -z $MODEL ] ; then
#echo "No model found!"
#exit 1;
#fi

if  [ -z $multisnvVCF ] ; then
echo "No input vcf found"
exit 1;
fi

if  [ -z $multisnvARFF ] ; then
echo "Did not find an input ARFF file."
exit 1;
fi


##DO NOT CHANGE ANYTHING AFTER THIS POINT, OTHER THAN FILTER NAMES!!!

REPO="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DBSNP=${REPO}/datasets/dbsnp_137_b37_excluding_sites_after129.vcf
DUKEBL=${REPO}/datasets/DukeBL.bed
DACBL=${REPO}/datasets/DacBL.bed
LCR=${REPO}/datasets/LCR_Li2014.bed
PON=${REPO}/datasets/CRUK_CI_PoN.vcf


new_name="all_flags"
finalVCF=${multisnvVCF%.vcf}_${new_name}.vcf
MLfilteredVCF=${multisnvVCF%.vcf}_AdaBoost.vcf
finalARFF=${multisnvARFF%.arff}_${new_name}.arff
INDEL_THRESH=0.05

outputVCF=${multisnvVCF}
outputARFF=${multisnvARFF}

#Run probabilistic filter to flag sites that are possibly germline
filter2="temp_germlineRisk"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter2}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter2}.arff

python ${REPO}/flagPossiblyGermline.py --bams ${BAMS} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF}

#Flag sites with asymmetric mapping qualities in ref/variant reads
filter3="asymmetricMapping"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter3}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter3}.arff

python ${REPO}/flagAsymmetricMapping.py --bams ${BAMS} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF}

#Flag sites overlapping dbSNP
filter4="dbSNP"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter4}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter4}.arff

python ${REPO}/flagOverlappingEntries.py --vcf ${DBSNP} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF} --filterFlag ${filter4}

#Flag sites overlapping your panel of normals - comment out if you don't have one!
filter5="PoN"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter5}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter5}.arff

python ${REPO}/flagOverlappingEntries.py --vcf ${PON} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF} --filterFlag ${filter5}

#Flag sites where more than a percentage of aligned reads spans an indel
filter6="NEARBY_INDELS"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter6}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter6}.arff

python ${REPO}/flagNearbyIndels.py  --bams $BAMS --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF} --arffout ${outputARFF} --threshold $INDEL_THRESH

filter="DacBL"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter}.arff

python ${REPO}/flagOverlappingEntries.py --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF} --arffout ${outputARFF} --filterFlag ${filter} --bed ${DACBL}

filter="DukeBL"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter}.arff

python ${REPO}/flagOverlappingEntries.py --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF} --arffout ${outputARFF} --filterFlag ${filter} --bed ${DUKEBL}

filter="LCR"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter}.arff

python ${REPO}/flagOverlappingEntries.py --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF} --arffout ${outputARFF} --filterFlag ${filter} --bed ${LCR}


if [ -z ${VALIDATED+x} ]

then :

else echo "Flagging validated SNVs with ${VALIDATED}"

filter6="Validated"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter6}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter6}.arff

python ${REPO}/flagOverlappingEntries.py --vcf ${VALIDATED} --fin ${inputVCF} --fout ${outputVCF} --arf\
f ${inputARFF}  --arffout ${outputARFF} --filterFlag ${filter6}
fi

mv ${outputVCF} ${finalVCF}
mv ${outputARFF} ${finalARFF}

rm ${multisnvVCF%.vcf}_temp*

if [ -z ${MODEL+x} ]; then

    python ${REPO}/predict.py --fin ${finalVCF} --arff ${finalARFF} --fout ${MLfilteredVCF}

else

python ${REPO}/predict.py --fin ${finalVCF} --arff ${finalARFF} --fout ${MLfilteredVCF} --model ${MODEL}

fi

