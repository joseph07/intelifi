# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 18:15:05 2016

@author: joseph07
"""

import vcf
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")

import os
from optparse import OptionParser
import filter_utils as filt
reload(filt)
import SequencingData as dt
reload(dt)
import pysam
import NearbyIndels as indel_utils
reload(indel_utils)
import multisnv_vcf_utils as utils
import argparse
from datetime import datetime


parser = argparse.ArgumentParser(description=""' Flags candidate somatic sites as "NEARBY_INDELS" when the fraction of reads spanning indels in an 11bp window '
                                                    'centered on the variant exceeds a user-specified threshold (--threshold).'
                                                    'In cases where the VCF reports variants across multiple samples, reads across all samples (including the normal) are pooled.' 
                                                    'This tool is generic and compatible with any VCF file. '
                                                    'The user inputs a VCF file (--fin) and the BAM files used during variant calling in the same order they are found in the VCF columns. '
                                                    'The output is another VCF file. '
                                                    ' If an input ARFF (--arff) and output ARFF (--arffout) file are also specified the fraction of reads '
                                                    'spanning indels is appended to the output ARFF file. '"", prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--fin', help='Input vcf', required=True)
parser.add_argument('--fout', help='Output vcf', required=True)
parser.add_argument('--bams', help='List of Bam files',  nargs='+', type=str, required=True)
parser.add_argument('--arff', help='Optional features file', required=False)
parser.add_argument('--arffout', help='Optional output features file. If this is included we will add to the arff file the number of indels found', required=False)
parser.add_argument('-t', '--threshold', help = "Flag if more than this fraction of reads have indels in an 11bp window centered on the read", required = False, default = 0.05)
options = parser.parse_args()


startTime = datetime.now()

if options.arff != None and options.arffout == None:
    sys.exit("You need to specify both an input and an output ARFF file")
    
elif options.arff == None and options.arffout != None:
    sys.exit("You need to specify both an input and an output ARFF file")

print "\nRunning",__file__
options.threshold = float(options.threshold)
filterFlag = 'NEARBY_INDELS'
sites = indel_utils.countIndelsInWindow(options.fin, options.bams, filterFlag)

filt.flagSites(list(sites[sites[filterFlag] > options.threshold].index), options.fout, options.fin, filterFlag)
filt.addVcfHeader('##FILTER=<ID=%s,Description="More than %s of all aligned reads have an indel within 11bp of variant">\n' %(filterFlag, options.threshold), 38, options.fout)
print "Feature computation completed."

if options.arff != None and options.arffout != None:
    filt.appendArff(sites, options.arff, filterFlag,options.arffout)

print "Time elapsed:", datetime.now() - startTime