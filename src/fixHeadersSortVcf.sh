grep '##' ${1} > xyz_t1.vcf
grep '#CHROM' ${1}| uniq > xyz_t4.vcf
grep -ve '#' ${1} > xyz_t2.vcf

cat xyz_t1.vcf xyz_t4.vcf xyz_t2.vcf > xyz_t3.vcf

grep '^#' xyz_t3.vcf > ${2}

for chr in {1..22} X Y
do
grep "^chr${chr}	" xyz_t3.vcf | sort -nk2 >> ${2}
done

rm xyz_t1.vcf
rm xyz_t2.vcf
rm xyz_t3.vcf
rm xyz_t4.vcf

