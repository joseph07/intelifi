# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 13:19:00 2016

@author: joseph07
"""
import csv
import random
import math
import pandas as pd
import numpy as np
import re

def loadDataset(filename):
    num_headers = findNumHeaders(filename)
    dataset = pd.DataFrame.from_csv(filename, header=num_headers-2)
    dataset.set_index(dataset.index + ":" + dataset["POS"].map(str),inplace= True)
    dataset = dataset[dataset.columns[1:]]
    dataset = dataset.convert_objects(convert_numeric=True)
    return dataset
 
def findNumHeaders(filename):
    count_headers = 1
    f = open(filename, "rb")    
    line = f.readline()
    while sum(line.count(x) for x in "#@%") > 0:
        count_headers= count_headers +1
        line = f.readline()     
    f.close() 
    return count_headers

def removeMissing(dataset):
    dataset=dataset.replace('?', np.nan)
    dataset.dropna(axis=0, how='any', inplace=True)
    return dataset

def onlyMaxVariantFreq(P001): 
    #print "Computing the maximum variant freq across all tumour samples"
    freq_cols = []
    for col in P001.columns:
        match = re.match(r"T\d{1,}_VARIANT_FREQ", col)
        if match:
            freq_cols.append(match.group())
    
    P001["T1_VARIANT_FREQ"] = P001[freq_cols].values.max(axis=1)
    for col in freq_cols:
        if col != "T1_VARIANT_FREQ":
            P001.drop(col, axis=1, inplace=True)
    return P001
