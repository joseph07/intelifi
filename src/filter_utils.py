# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 21:28:44 2016

@author: joseph07
"""

import vcf 
import os
import numpy as np
import pandas as pd
import subprocess

def getVCFsubset(inputVCF, outputVCF, targetEvents):
    vcf_reader = vcf.Reader(open(inputVCF))
    vcf_writer = vcf.Writer((open(outputVCF, 'w')), vcf_reader)
    targetEvents = list(targetEvents)
    for record in vcf_reader:
        if targetEvents.count(record.CHROM+":" + str(record.POS)) > 0:
            vcf_writer.write_record(record)


#Gold pos has a column of chrom and a column of pos ,dataframe is in the form "chrom:pos" as the index, and just ones in the first column
def getChromPosDataFrame(goldPos, filterFlag):
    all_gold = open(goldPos)
    all_gold = all_gold.readlines()
    
    goldSites = []
    for line in all_gold:
        line = line.split('\t')
        goldSites.append(line[0] + ":" + str(line[1]))
            
    return pd.DataFrame(np.ones(len(goldSites)), index=goldSites, columns=[filterFlag])

#Template vcf is required as we need to pull metadata from this - it should be the original vcf file - before flagging
def flagSites(sites, out_vcf, template_vcf, filterFlag):
    #Write header    
    vcf_reader = vcf.Reader(open(template_vcf, 'r'))
    vcf_writer = vcf.Writer((open(out_vcf, 'w')), vcf_reader)
    
    for record in vcf_reader:
        if sites.count(record.CHROM+":" + str(record.POS)):
            record.FILTER.append(filterFlag)
            record.FILTER = ";".join(record.FILTER)
        
        vcf_writer.write_record(record)
        
    vcf_writer.close()
    return

#Rownum indicates where we want to enter the new headerLine.
def addVcfHeader(headerLine, rowNum, out_vcf):
    #Add new line to header
    f = open(out_vcf, "r")
    contents = f.readlines()
    f.close()
    contents.insert(rowNum, headerLine)

    f = open(out_vcf, "w")
    contents = "".join(contents)
    f.write(contents)
    f.close()
    return

def passFilter(filt, passFilters):
     filt = ";".join(filt)
     return passFilters.count(filt) > 0
     
def appendArff(sites, arff, filterFlag, outputARFF):
    output = open(outputARFF, 'w')
    
    with open(arff) as arff_lines:
        for line in arff_lines:
            line = line.replace('\n','')
            if line[0] != '#' and line[0] != "%" and line[0] != '@':
                temp = line.split(",")
                pos = temp[0] + ":" + temp[1]
                if pos in sites.index:
                    value = sites.loc[pos][0]      
                else:
                    value = "?"

                line = ",".join((line, str(value) + '\n'))
            
            elif line.split(',')[0] == "#CHROM":
                line = ",".join((line, filterFlag + "\n"))

            else:
                line = line + '\n'

            output.write(line)
    output.close()
    print "Feature has been saved."
    return
        
def getOverlappingSites(inputvcf, largevcf, filterFlag):
    arg1 = '%s' %(inputvcf)
    arg2 = '%s' %(largevcf)
    
    bashCommand = "sort " +  arg1 + " "  + arg2 + "  | grep -ve '^#' | cut -f 1,2  |uniq -d  " 
 
    sharedPositions = subprocess.check_output(bashCommand, shell=True)
    sharedPositions = sharedPositions.split('\n')

    sites = []
    for i in sharedPositions[0:-1]:
        name = i.split('\t')
        sites.append(name[0]+ ":"+name[1])

    return pd.DataFrame(np.ones(len(sites)), index=sites, columns=[filterFlag])

#Function will append arff so that ANY arff entry that is not included in "sites" will get a feature value of 0, otherwise it will be 1.
def appendArffIfPresent(sites, arff, colname, outARFF):
    output = open(outARFF, 'w')

    with open(arff) as arff_lines:
        for line in arff_lines:
            line = line.replace('\n','')
            if line[0] != '#' and line[0] != "%" and line[0] != '@':
                temp = line.split(",")
                pos = temp[0] + ":" + temp[1]
                
                if pos in sites.index:
                    value = sites.loc[pos][0]
                    
                else:
                    value = 0
                

                line = ",".join((line, str(value) + '\n'))
            
            elif line.split(',')[0] == "#CHROM":
                line = ",".join((line, colname + "\n"))
            
            else:
                line = line + '\n'
            
            output.write(line)
    output.close()
    print "Feature has been saved."
    return

def getSitesOverlappingInterval(inputVCF, bedFile, filterFlag):
    intervals = pd.read_csv(bedFile,sep='\t', header=-1, low_memory=False)
    sites = []
    vcf_reader = vcf.Reader(open(inputVCF))
    intervals=pd.DataFrame.from_csv(bedFile, sep='\t')
    
    chromStart = "start"
    ind = 0
    for record in vcf_reader:

        if chromStart != record.CHROM:
            chrom_intervals = intervals[intervals.index == record.CHROM]
            chromStart = record.CHROM
            ind = 0
            
        while (ind < len(chrom_intervals)):
            interval = chrom_intervals.iloc[ind].values
            start = interval[0]
            end = interval[1]
            
            if record.POS >= start and record.POS <=end:            
                sites.append(record.CHROM+":" + str(record.POS))
                break
            
            elif record.POS > end:
                ind = ind + 1
            elif end > record.POS:
                break
            
            else:
                pass

    return pd.DataFrame(np.ones(len(sites)), index=sites, columns=[filterFlag])
