# -*- coding: utf-8 -*-
"""
Created on Sat Nov 21 20:29:26 2015

@author: joseph07
"""
import math
import numpy as np
import pandas as pd
import pysam
    
class Maf(object): 
    def __init__(self, allele, freq):
        self.allele = allele
        self.freq = freq
        self.__isValid = self.allele in ['A','C', 'G', 'T']  
        self.__isEmpty = not self.allele

    def isEmpty(self):
        return self.__isEmpty   
    
    def isValid(self):
        return self.__isValid
        
class Data:
    def __init__(self, reads):
        self.reads = reads
        self.bcounts = self.get_allele_counts()
        
    def get_allele_counts(self):
        bcounts = []
        for allele in ['A','C','G','T']:
            bcounts.append(sum(self.reads['base'] == allele))
        bcounts = np.array(bcounts,dtype= int)
        return bcounts
     
    def filter_data(self, mapping_quality = 20, base_quality= 20):
        reads = self.reads
        reads = reads[reads['mapq']>= mapping_quality]
        reads = reads[reads['baseq']>= base_quality]
        filtered_data = Data(reads)
        return filtered_data
        
    def compute_marginal_likelihood(self, Lambda, zeta):
        K = sum(Lambda)
        f_map = self.compute_approximate_MAP_freq(Lambda, zeta)
        bases='ACGT'
        log_likelihood = 0 
        if len(self.reads) < 1:
            return 0
            
        else:
            for index,row in self.reads.iterrows():
                base = row['base']
                qual = row['baseq']
                e = convert_Phred_scaled_qual(qual)
                ind = bases.find(base)
                log_likelihood = log_likelihood + (math.log((f_map[ind] *(1-e)+(1-f_map[ind])*e/3)))
             
            log_likelihood = log_likelihood - 0.5* K* math.log(sum(self.bcounts))
            return log_likelihood
            
    def compute_approximate_MAP_freq(self, Lambda, zeta):
        valid_counts = self.bcounts * Lambda
        num = (valid_counts + zeta - 1) 
        K = sum(Lambda)
        den = ( sum(valid_counts) + zeta*K - K)
        f_map = (np.array(num, dtype=float)/den) * Lambda
        return f_map       
        
        
class Loc(object):
    def __init__(self, chrom, position):
        assert type(chrom) == str
        assert type(position) == int
        self.chrom = chrom
        self.position = position


def convert_Phred_scaled_qual(qual):
    return 10**(-qual*0.1)  
     
def getReads(pileup, loc):
    exact_pos = loc.position - 1
    pileupcolumns = pileup(loc.chrom, exact_pos, exact_pos + 1 )
    for pileupcolumn in pileupcolumns:
        #print ("\ncoverage at base %s = %s" % (pileupcolumn.pos, pileupcolumn.n))
        if pileupcolumn.pos == exact_pos:
            #TDOO 3000 is the maximum acceptable coverage for this code.This means it cannot be used on deep sequencing datasets
            if len(pileupcolumn.pileups) > 3000:
                    raise ValueError("Skipping position %s %s due to excessive depth (%s)" %(loc.chrom, loc.position, len(pileupcolumn.pileups)))
            else:
                reads = []
                for pileupread in pileupcolumn.pileups:
                    if not pileupread.is_del:
                        base_qual = pileupread.alignment.query_qualities[pileupread.query_position]
                        map_qual = pileupread.alignment.mapping_quality
                        base = pileupread.alignment.query_sequence[pileupread.query_position]
                        read = tuple( (base, base_qual, map_qual ))
                        reads.append(read)
                
                if len(reads) < 1:
                    raise ValueError("Skipping position %s %s due to zero coverage (%s)" %(loc.chrom, loc.position, len(pileupcolumn.pileups)))
                    
                else:
                    return pd.DataFrame.from_records(reads, columns=['base', 'baseq', 'mapq'])       
 
 
def collectMultisampleData(bams, loc, mapq, baseq):
    ms_data = []
    for i,bam in enumerate(bams):
        try:
            data = Data(getReads(bam.pileup, loc))    
        except:
            print "Encountered a problem while collecting reads from %s at position %s %s"  %(bam.filename, loc.chrom, loc.position )
            raise            
        else:
            ms_data.append(data.filter_data(base_quality = baseq, mapping_quality = mapq)  )
        
    return ms_data
