# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 12:13:54 2016

@author: joseph07
"""
import filter_utils as filt_util
import vcf 
import multisnv_vcf_utils as utils
reload(filt_util)
reload(utils)
import GermlineRisk as gr
import SequencingData as dt
reload(dt)
reload(gr)
import pysam
import pandas as pd

def getMaf(record, gmaf_col):
    gmaf = record.INFO['CSQ'][-1].split('|')[gmaf_col]
    temp = gmaf.split(':')
    
    if len(temp) == 1:
        return dt.Maf('','')
        
    elif len(temp) == 2:
        allele = temp[0]
        freq = float(temp[1])
        return dt.Maf(allele, freq) 
        
    elif len(temp) == 3:
        return dt.Maf('','')
#TODO : How should we handle these entries?   -:0.0487&G:0.005 and -:0.18       
#        print "Unusual maf " , gmaf 
#        reparsed = temp[1].split('&')
#        if len(reparsed) == 2:
#            allele = reparsed[1]
#            freq = float(temp[2])
#            #To check if this actually every happens
#            raise StandardError("Unexpected MAF parsed!", temp)    
#            print "MAF recovered" , temp
#            return dt.Maf(allele, freq)
    else:
        raise StandardError("Unexpected MAF found!", gmaf)    

#This is to get the maf from another vep entry -specific to race
def findMAF_1kgcol(infos, race):
    csq_header = (infos['CSQ']).desc.split(':')[1]
    gmaf_col = csq_header.split('|').index('%s_MAF' % race)
    return gmaf_col
    
def findGMAFcol(infos):
    try:
        csq_header = (infos['CSQ']).desc.split(':')[1]     
    except:
        raise KeyError("This tool is only compatible with VEP annotated VCFs")
        
    gmaf_col = csq_header.split('|').index('GMAF')
    return gmaf_col

#TODO://What about these cases? T:0.13 and the reference is actually T, how do we interpret these?!
#chr12	64669019	.	T	A	12.2011	CLUSTERED_READS	NS=11;DISTR=|T|AT|T|AT|T|AT|T|AT|T|AT|AT|;SB=1.0;CSQ=A|ENSG00000185306|ENST00000541802|Transcript|non_coding_transcript_exon_variant&non_coding_transcript_variant|348|||||rs5798743&rs112622378||-1|C12orf56|HGNC|26967|retained_intron||||||||||3/4|||ENST00000541802.1:n.348N>T||T:0.3434|-:0.56|-:0.76|-:0.49|-:0.79|-:0.516183|-:0.797911|||||||,A|ENSG00000185306|ENST00000536975|Transcript|intron_variant&non_coding_transcript_variant||||||rs5798743&rs112622378||-1|C12orf56|HGNC|26967|processed_transcript|||||||||||5/7||ENST00000536975.1:n.499+59N>T||T:0.3434|-:0.56|-:0.76|-:0.49|-:0.79|-:0.516183|-:0.797911|||||||,A|ENSG00000185306|ENST00000543942|Transcript|intron_variant||||||rs5798743&rs112622378||-1|C12orf56|HGNC|26967|protein_coding|||CCDS61182.1|ENSP00000446101|CL056_HUMAN||UPI0001C0B37D||||10/12||ENST00000543942.2:c.1509+59N>T||T:0.3434|-:0.56|-:0.76|-:0.49|-:0.79|-:0.516183|-:0.797911|||||||,A|ENSG00000185306|ENST00000535515|Transcript|upstream_gene_variant||||||rs5798743&rs112622378|4666|-1|C12orf56|HGNC|26967|processed_transcript|||||||||||||||T:0.3434|-:0.56|-:0.76|-:0.49|-:0.79|-:0.516183|-:0.797911|||||||,A|ENSG00000243024|ENST00000535684|Transcript|intron_variant&non_coding_transcript_variant||||||rs5798743&rs112622378||1|RPS11P6|HGNC|36693|processed_transcript|YES||||||||||2/4||ENST00000535684.1:n.319+47254N>A||T:0.3434|-:0.56|-:0.76|-:0.49|-:0.79|-:0.516183|-:0.797911|||||||,A|ENSG00000185306|ENST00000333722|Transcript|intron_variant||||||rs5798743&rs112622378||-1|C12orf56|HGNC|26967|protein_coding|YES||CCDS44935.1|ENSP00000329698|CL056_HUMAN||UPI00001D7985||||8/10||ENST00000333722.5:c.1029+59N>T||T:0.3434|-:0.56|-:0.76|-:0.49|-:0.79|-:0.516183|-:0.797911|||||||,A|ENSG00000185306|ENST00000542397|Transcript|intron_variant&non_coding_transcript_variant||||||rs5798743&rs112622378||-1|C12orf56|HGNC|26967|processed_transcript|||||||||||4/6||ENST00000542397.1:n.375+59N>T||T:0.3434|-:0.56|-:0.76|-:0.49|-:0.79|-:0.516183|-:0.797911|||||||	GT:A:GQ:SS:BCOUNT:DP	0/0:T:100.0:0:0,0,0,12:12	0/1:AT:19.4939:2:1,0,0,9:10	0/0:T:100.0:0:0,0,0,18:18	0/1:AT:100.0:2:3,0,0,8:11	0/0:T:19.4939:0:0,0,0,9:9	0/1:AT:100.0:2:2,0,0,19:21	0/0:T:12.3045:0:0,0,0,7:7	0/1:AT:100.0:2:2,0,0,8:10	0/0:T:100.0:0:0,0,0,22:22	0/1:AT:100.0:2:2,0,0,16:18	0/1:AT:100.0:2:3,0,0,13:16
