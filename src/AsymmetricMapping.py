# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 23:24:37 2016

@author: joseph07
"""

import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
#Requires PyVCF
import vcf
import filter_utils as filt_util
reload(filt_util)
import SequencingData as dt
reload(dt)
import multisnv_vcf_utils as utils
reload(utils)
import pysam
import pandas as pd

def computeMappingQualityDiffPerSomaticSite(inputVCF, bam_names, parameters):
    bams = []
    for i in bam_names:  
        bams.append(pysam.AlignmentFile(i, "rb"))
        
    f = open(inputVCF,'r')
    vcf_reader = vcf.Reader(f)
    
    if int(vcf_reader.metadata['Number of samples'][0]) != len(bams):
        raise ValueError("Entries in vcf don't match input bam files")
        
    sites = []
    sites_values = []
    for record in vcf_reader:
        if utils.isValidSomatic(record.samples): #so it won't run on mutations of a heterozygous normal - to do this use something like find nonubiq alleles, then compare the nonubiq allele to average of the rest?
            try:            
                ms_data = dt.collectMultisampleData(bams,dt.Loc(record.CHROM, record.POS),parameters['mapq'],parameters['baseq']) 
            except Exception as err:
                print err.args
            else:
                observedEvent = [i['A'] for i in record.samples]
                normal =  observedEvent[0]
                if len(normal) == 1:
                    ref = normal
                    variant = utils.determineVariant(observedEvent)
    
                    count_variant_reads = 0
                    count_ref_reads = 0
                    total_variant_MAPQ = 0
                    total_ref_MAPQ= 0
                    
                    for i, sample in enumerate(observedEvent):
                        reads = ms_data[i].reads
                        ref_reads = reads[reads['base'] == ref]  
                        var_reads = reads[reads['base'] == variant]
                        
                        count_variant_reads = count_variant_reads + len(var_reads)
                        count_ref_reads = count_ref_reads + len(ref_reads) 
                        
                        total_variant_MAPQ = total_variant_MAPQ + var_reads['mapq'].sum()
                        total_ref_MAPQ = total_ref_MAPQ + ref_reads['mapq'].sum()
                        
                    if count_variant_reads > 0 and count_ref_reads > 0:
                        var_reads_ave_qual = float (total_variant_MAPQ) / count_variant_reads
                        ref_reads_ave_qual =float (total_ref_MAPQ) / count_ref_reads
                        mapq_diff = ref_reads_ave_qual - var_reads_ave_qual
            
                        sites.append((record.CHROM + ":" + str(record.POS)))
                        sites_values.append(mapq_diff)

    for i in bams:
        i.close()
    
    return pd.DataFrame(sites_values, index = sites, columns=['mapq_diff'])
    

