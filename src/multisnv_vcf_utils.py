# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 13:04:30 2016

@author: joseph07
"""

import numpy as np
import pandas as pd 
import filter_utils as filt_util
import vcf
import matplotlib.pyplot as plt
import matplotlib as mpl

def plotBinaryMatrix(distribution_sorted, samples, xlabel, ylabel):
    fig, ax = plt.subplots()
    fig.set_size_inches(6, 10.5, forward=True)
    # define the colors
    cmap = mpl.colors.ListedColormap(['beige', 'coral'])
    # create a normalize object the describes the limits of each color
    bounds = [0., 0.5, 1.]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    ax.imshow(distribution_sorted, interpolation='none', cmap=cmap, norm=norm, aspect='auto')
    plt.xlabel(xlabel, fontsize=16)
    plt.ylabel(ylabel, fontsize=16)
    plt.xticks(rotation=30)
    plt.grid('on')
    plt.xticks(range(distribution_sorted.shape[1]), samples, fontsize=14)
    plt.tick_params(axis="both", labelsize=14)

    return fig

#Will only keep rows duplicated more than threshold number of times
def orderBinaryMatrixRows(distributions, threshold,samples):    
    numColumns =  len(distributions.columns)
    sortedIndex = getAllBinaryRows(numColumns)
    return sortCleanupBinaryMatrixRowsByIndex(distributions, sortedIndex, threshold, samples)

#Get all combinations of binary rows
def getAllBinaryRows(numberOfSamples):
    largest_num = (2**numberOfSamples) 
    candidate_rows =[]
    for i in range(largest_num):
        row = format(i, '0%sb' %numberOfSamples)
        row = [i for i in row]
        candidate_rows.append(np.array(row, dtype=float))
    
    
    sortedIndex = []
    for numZeros in range(0,numberOfSamples): 
        for row in candidate_rows:
            if sum(row == 0)== numZeros:
                sortedIndex.append(row)
    
    return (sortedIndex)
            

def getParameterFromVCFheader(vcfParameterEntry, vcf_file):
    f = open(vcf_file)
    vcf_reader = vcf.Reader(f)
    return num(vcf_reader.metadata[vcfParameterEntry][0])
        
def convertAllelicCompToBinaryMatrix(vcf_file, passFilters):
    f = open(vcf_file)
    vcf_reader = vcf.Reader(f)
    distributions = []
    pos = []
    for record in vcf_reader:
        if isValidSomatic(record.samples):
            if passFilters == None:
                includeSite = True
            elif passFilters != None:
                includeSite = filt_util.passFilter(record.FILTER, passFilters)
                    
            if includeSite:
                distr = np.zeros(len(record.samples))
                normal = record.samples[0]['A'] 
                if len(normal) == 1:
                    pos.append(record.CHROM + ":" + str(record.POS))
                    for i in range(1,len(record.samples)):
                        tumour = record.samples[i]['A']
                        if tumour != normal:
                            distr[i] = 1
                    distributions.append(distr)
                else:
                    pass
    
    return(pd.DataFrame.from_records(distributions, index =pos))

#Just group
def sortBinaryMatrixRows(binary_matrix):
    sorted_index = []
    unique_rows = np.vstack({tuple(row) for row in binary_matrix.values})
    for row in unique_rows:
        #For this unique_row check which rows in the binary_matrix match it
        t1 = [all(binary_matrix.iloc[i].values == row) for i in range(len(binary_matrix))]
        #save the index of these matrices
        for i in range(len(binary_matrix)):
            if t1[i] == True:
                sorted_index.append(binary_matrix.index[i])
    
    sortedMatr = []
    for row in (sorted_index):
        #For each position in the sorted positions , find the corresponding binary row and append it.
        sortedMatr.append(np.array(binary_matrix[binary_matrix.index == row].values))
        
    sortedMatr = np.vstack(sortedMatr)
    return (pd.DataFrame.from_records(sortedMatr, index =sorted_index))
    
def sortCleanupBinaryMatrixRowsByIndex(binary_matrix, input_index, toler, samples):
    sorted_index = []
    for row in input_index:
        #Take row from sorted index
        #row = [i for i in row]
        #row=np.array(row, dtype=float)
        #For this row check which rows in the binary_matrix match it
        t1 = [all(binary_matrix.iloc[i].values == row) for i in range(len(binary_matrix))]
        
        if(sum(t1) > toler):
            print "Found " , sum(t1) , "rows as ", row
        #save the index of these matrices
            for i in range(len(binary_matrix)):
                if t1[i] == True:
                    sorted_index.append(binary_matrix.index[i])
        else:
            print "Throwing out ", sum(t1), "rows as ", row
    
    sortedMatr = []
    for row in (sorted_index):
        #For each position in the sorted positions , find the corresponding binary row and append it.
        sortedMatr.append(np.array(binary_matrix[binary_matrix.index == row].values))
        
    sortedMatr = np.vstack(sortedMatr)
    return (pd.DataFrame.from_records(sortedMatr, index =sorted_index, columns=samples))



def findEventsMatchingDistr(target_distr, distributions):
    isMatching = [all(distr.astype(int) == target_distr.astype(int)) for distr in distributions.values]
    return distributions.index[isMatching]

def num(s):
    try:
        return int(s)
    except ValueError:
        return float(s)
        
def decimal2binary(dec):
    binary = np.zeros(4) #[0,0,0,0]
    i=0
    while(dec > 0 and dec < 16):
        if( dec & 1):
            binary[3-i] = 1
        i = i+1
        dec = dec >> 1
            
    return binary

def dec2binary(dec):
    binary= []
    
    while dec > 0:
        if dec & 1:
            binary.append(1)
        else:
            binary.append(0)
        
        dec = dec >> 1
    
    binary.reverse()
    return np.array(binary)
        
        
def is_allele_valid(input_str):
    bases = ['A','C','G','T']
    return bases.count(input_str) > 0

def determineVariant(observedEvent):
    events = set(observedEvent)
    alleles = "".join(events)
    
    for i in alleles:
        if alleles.count(i) == 1:
            return i
    
    raise ValueError("This is not a somatic event" , observedEvent)
    
def getAlleleCompAsBinaryVector(allelic_comp): 

    binary_matrix = [0,0,0,0]
    bases = 'ACGT'
    for i in allelic_comp:
        binary_matrix[bases.index(i)] = 1

    return binary_matrix

def isSomatic(samples):
    genotypes = []
    for i in range(len(samples)):
        genotypes.append(samples[i]['A'])
    
    return len(set(genotypes)) > 1
    
def isValidSomatic(samples):
    genotypes = []
    for i in range(len(samples)):
        genotype = samples[i]['A']
        #Exclude multiallelic (i.e. mutations on germline het sites) and more than 2 mutations
        if len(genotype) > 2:
            return False
            
        genotypes.append(genotype)
    
    return len(set(genotypes)) == 2