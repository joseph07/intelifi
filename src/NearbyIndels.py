# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 18:16:07 2016

@author: joseph07
"""

import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
#Requires PyVCF
import vcf
import pysam
import pandas as pd

def countIndelsInWindow(inputVCF, bamNames, filterFlag):
    bams = []
    for i in bamNames: 
        bams.append(pysam.AlignmentFile(i, "rb"))   

    sites = [] 
    sites_values = []
    vcf_reader = vcf.Reader(open(inputVCF))
    for record in vcf_reader:
        chrom =   record.CHROM
        pos = record.POS
        exact_pos = pos - 1

        indels = 0
        adjacent_indels = 0
        total_reads = 0

        for bam in bams:
            for read in bam.fetch(chrom, exact_pos, exact_pos + 1):
                total_reads = total_reads +1
                positions = read.get_reference_positions()
                #If exactpos is missing from positions then we have a deletion
                if positions.count(exact_pos) == 0:
                    indels = indels + 1
                
                else:
                    #Find the window of interest
                    pivot = positions.index(exact_pos)
                    #If at the start
                    if(pivot - 5) < 0:
                        start = 0
                        end = 10  
                    #if at the end if
                    elif(pivot + 5) > len(positions)-1:
                        end = len(positions) - 1
                        start = len(positions) - 10 
                    else:
                        start = pivot - 5
                        end = pivot + 5
                    
                    cigars = []
                    for cigar in read.cigartuples:
                        cigars.append([cigar[0]]* cigar[1])
                        
                    #Flatten the list
                    cigars = sum(cigars, [])
                    adjacent_cigars = cigars[start:end]
                    #If insertion or deletion (1,2) cigar
                    if adjacent_cigars.count(1) or adjacent_cigars.count(2):
                        adjacent_indels = adjacent_indels + 1
                    
        sites.append((record.CHROM + ":" + str(record.POS)))
        sites_values.append(float(adjacent_indels + indels)/total_reads)

      
    for i in bams:
        i.close()  
        
    indelInfo = pd.DataFrame(sites_values, index = sites, columns=[filterFlag])
    
    return indelInfo
                       

