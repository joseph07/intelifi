# -*- coding: utf-8 -*-
"""
Created on Sat Nov 21 14:37:52 2015

@author: joseph07
"""

import math
import numpy as np
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
import pysam
import pandas as pd
import vcf
import SequencingData as dt
reload(dt)
import math
import filter_utils as filt_util
reload(filt_util)
import VEPvcf as vep
import multisnv_vcf_utils as utils
reload(utils)

def computeGermlineRiskSites(inputVCF, bam_names, parameters, filterFlag):
    bams = []

    for i in bam_names:
        bams.append(pysam.AlignmentFile(i, "rb"))
    
    f = open(inputVCF,'r')
    vcf_reader = vcf.Reader(f)
    
    if int(vcf_reader.metadata['Number of samples'][0]) != len(bams):
        raise ValueError("Entries in vcf don't match bam files: Expected %s Bam files and only received %s" %(vcf_reader.metadata['Number of samples'][0], len(bams)))
        
    gmaf_col = vep.findGMAFcol(vcf_reader.infos)
    #race = 'EUR'
    #gmaf_col2 = findMAF_1kgcol(vcf_reader.infos, race)
    sites = []
    sites_values = []
    for record in vcf_reader:
        if utils.isValidSomatic(record.samples):
            maf = vep.getMaf(record, gmaf_col)
            if maf.isValid():
                if record.ALT.count(maf.allele) > 0:
                    try:
                        logratio = computeGermlineSomaticLogRatio(bams, record, parameters, maf)
                    except ValueError as err:
                        print err.args
                    except ZeroDivisionError as err:
                        print err.args   
                    #This is executed if no exception is thrown (python syntax)
                    else:
                        sites.append(record.CHROM + ":" + str(record.POS))
                        sites_values.append(logratio)

    for i in bams:
        i.close()
    
    return pd.DataFrame(sites_values, index = sites, columns=[filterFlag])
    
#This uses the same maf for all sites - (no vep annotation) 
def computeGermlineSomaticPosteriorLogRatio(inputVCF, bam_names, parameters, default_maf, filterFlag):
    bams = []

    for i in bam_names:
        bams.append(pysam.AlignmentFile(i, "rb"))
    
    f = open(inputVCF,'r')
    vcf_reader = vcf.Reader(f)
    
    if int(vcf_reader.metadata['Number of samples'][0]) != len(bams):
        raise ValueError("Entries in vcf don't match bam files")
        
    sites = []
    sites_values = []
    for record in vcf_reader:
        if utils.isValidSomatic(record.samples):
            if len(record.ALT) == 1 :
                    maf = dt.Maf(record.ALT, default_maf)
                    try:
                        logratio = computeGermlineSomaticLogRatio(bams, record, parameters, maf) 
                    except ValueError as err:
                        print err.args
                    except ZeroDivisionError as err:
                        print err.args
                    else:
                        sites.append(record.CHROM + ":" + str(record.POS))
                        sites_values.append(logratio)
            
    for i in bams:
        i.close()
        
    return pd.DataFrame(sites_values, index = sites, columns=[filterFlag])

def computeGermlineSomaticLogRatio(bams, record, parameters, maf):
    try:
        ms_data = dt.collectMultisampleData(bams, dt.Loc(record.CHROM, record.POS), parameters['mapq'], parameters['baseq'])
    except:
        raise

    else:
        observedEvent = [i['A'] for i in record.samples]
        germlineEvent = getNearestGermline(observedEvent)    
        
        loglikelihoodRatio = getGermlineSomaticLogLikelihoodRatio(germlineEvent, observedEvent,  ms_data, parameters['zetaN'], parameters['zetaT'])
        
        try:
            priorRatio = getGermlineSomaticPriorRatio(germlineEvent, observedEvent, maf, parameters['mut'])
            
        except:
            print "Exception raised while trying to computer Germline to Somatic Prior Ratio"
            raise
        
        else:
            logratio = loglikelihoodRatio + math.log(priorRatio)
            return logratio;     
        
def getNearestGermline(observedEvent):
    lengths = [len(i) for i in observedEvent]
    germline = observedEvent[lengths.index(max(lengths))]
    return [germline for i in observedEvent]
    
def getGermlineSomaticLogLikelihoodRatio(germlineEvent, observedEvent, ms_data, zetaN , zetaT):
    log_likelihood_observed = computeLogLikelihood(observedEvent, ms_data, zetaN, zetaT)
    log_likelihood_germline = computeLogLikelihood(germlineEvent, ms_data, zetaN, zetaT)
    return (log_likelihood_germline - log_likelihood_observed)
       
def getGermlineSomaticPriorRatio(germlineEvent, observedEvent, maf, mut): 
    P_N_obs = getP_N(observedEvent[0], maf)
    P_N_germ = getP_N(germlineEvent[0], maf)
    
    phi_N_obs = phiHyperparams_given_N(utils.getAlleleCompAsBinaryVector(observedEvent[0]), mut)    
    phi_N_germ = phiHyperparams_given_N(utils.getAlleleCompAsBinaryVector(germlineEvent[0]), mut)    
    
    n_obs = getCounts(observedEvent[1:])
    n_germ = getCounts(germlineEvent[1:])
    
    prior_T1_TN_given_N_obs = evaluateDirMulti(n_obs, phi_N_obs)
    prior_T1_TN_given_N_germ = evaluateDirMulti(n_germ, phi_N_germ)
    
    try:
        ratio = (P_N_germ*prior_T1_TN_given_N_germ) / (P_N_obs * prior_T1_TN_given_N_obs)
    except ZeroDivisionError:
        raise
    else:
        return ratio

def computeLogLikelihood(event, ms_data, zetaN, zetaT):
    normal_comp = utils.getAlleleCompAsBinaryVector(event[0])
    normal_log_likelihood =   ms_data[0].compute_marginal_likelihood(normal_comp, zetaN)
    log_likelihood = normal_log_likelihood
    
    #This will fail if ms_data and event don't have the same size
    for i in range(1, len(event)):
        allele_comp = utils.getAlleleCompAsBinaryVector(event[i])
        log_likelihood = log_likelihood +  ms_data[i].compute_marginal_likelihood(allele_comp, zetaT)
    return log_likelihood    
    

def phiHyperparams_given_N(normal_state, mut):
    #This is the range of all possible tumour states in binary format
    B_tumour_states = [utils.decimal2binary(i) for i in  range(1,15)]
    
    dist = [i - normal_state for i in B_tumour_states]
    abs_dist =  [sum(abs(i)) for i in dist ]
    probs = np.zeros(len(B_tumour_states))
    for i,j in enumerate(abs_dist):
        if(j == 0):
            probs[i] = 0
            zeroInd = i 
        else:
            probs[i] = mut**(int(j))
            
    probs[zeroInd] = 1-sum(probs)
    return probs
        
def getP_N(normal_state, maf):
    maf.freq = float(maf.freq)
    if(len(normal_state) == 1):
        
        if(maf.allele == normal_state):
            prior_normal = maf.freq * maf.freq
            
        else:
            prior_normal = (1-maf.freq)* (1-maf.freq)
    
    elif(len(normal_state) ==2):
        prior_normal = 2* maf.freq * (1-maf.freq)
    
    else:
        raise ValueError("Should not be seeing this normal: " + normal_state)
        
    return prior_normal

def getCounts(tumour_comps):
    
    B_tumour_states = [utils.decimal2binary(i) for i in  range(1,15)]
    tumour_states = [utils.getAlleleCompAsBinaryVector(i) for i in tumour_comps]
    
    n = np.zeros(len(B_tumour_states))
    for index,array in enumerate(B_tumour_states):
        n[index] = list(tumour_states).count(list(array))
        
    return n
    
#n is a vector holding counts per state, and phi are the hyperparameters for each discrete state (i.e. they are the parameters of the dirichlet prior) 
def evaluateDirMulti(n, phi): 
    prob = 1
    if(len(n)!= len(phi)):
        raise ValueError("The dimension of counts and hyperparameters of the dirichlet-multinomial don't match!" )

    else:
        for i in range(len(phi)):
            prob = prob * (math.gamma(n[i] + phi[i])/math.gamma(phi[i]))
            
        prob = prob * math.gamma(sum(phi))/math.gamma(sum(phi) + sum(n))
        return prob
    
   

