# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 13:01:55 2016

@author: joseph07
"""
import vcf
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
import filter_utils as filt
import pysam
import AsymmetricMapping as asy
import multisnv_vcf_utils as utils
from datetime import datetime
import argparse

parser = argparse.ArgumentParser(description=""' Flags candidate somatic sites as "ASYMMETRIC_MAPPING" when the difference in the average mapping quality between '
                                                ' reads supporting the reference and reads supporting the variant exceeds a user-specified threshold (--threshold).'
                                                ' Candidate somatic sites where the normal sample is heterozygous or with more than one mutation'
                                                ' per locus are ignored. In cases of multi-sample somatic variant calling, reads across all mutated samples'
                                                ' are pooled and the difference in the average mapping quality is computed using the pooled reads.'
                                                ' The tool is only compatible with multiSNV produced VCF files as it requires an allelic composition set to be reported.' 
                                                ' Usage: Input a multiSNV VCF file (--fin) and the BAM files in the same order that was used to run multiSNV. '
                                                ' The output is another VCF file. If an input ARFF (--arff) and output ARFF (--arffout) file are also specified the difference in the average mapping quality '
                                                ' is appended to the output ARFF file. Any candidate somatic sites that are ignored are appended by a "?" instead.'"", prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--fin', help='Input VCF', required=True)
parser.add_argument('--fout', help='Output VCF', required=True)
parser.add_argument('--bams', help='List of BAM files',  nargs='+', type=str, required=True)
parser.add_argument('--arff', help='Optional input features file. If this is included we will add to the arff file the difference in average mapping quality for all valid somatic sites', required=False)
parser.add_argument('--arffout', help='Optional output features file. If this is included we will add to the arff file the difference in average mapping quality for all valid somatic sites', required=False)
parser.add_argument('--threshold', help = "Filter if the difference in average mapping quality of reference and variant reads exceeds this threshold", required = False, default = 20)
options = parser.parse_args()


startTime = datetime.now()

if options.arff != None and options.arffout == None:
    sys.exit("You need to specify both an input and an output ARFF file")
    
elif options.arff == None and options.arffout != None:
    sys.exit("You need to specify both an input and an output ARFF file")
    
print "\nRunning",__file__

options.threshold = utils.num(options.threshold)
parameters = {'mapq':0, 'baseq':20, 'threshold': options.threshold}
sites = asy.computeMappingQualityDiffPerSomaticSite(options.fin, options.bams, parameters)

filterFlag = 'ASYMMETRIC_MAPPING'

filt.flagSites(list(sites[sites['mapq_diff'] > options.threshold].index), options.fout, options.fin, filterFlag)
filt.addVcfHeader('##FILTER=<ID=%s,Description="Difference in average mapping quality of reference and variant reads is more than %f">\n' %(filterFlag,options.threshold), 38, options.fout)

print "Feature computation completed."
if options.arff != None and options.arffout != None:
    #print "\nGenerating new ARFF file:", options.arffout
    filt.appendArff(sites, options.arff, filterFlag, options.arffout)

print "Time elapsed:", datetime.now() - startTime