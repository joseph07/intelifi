# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 17:09:38 2016

@author: joseph07
"""
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
#Requires PyVCF
import vcf
import os
import filter_utils as filt_util
reload(filt_util)
import argparse


parser = argparse.ArgumentParser(description=""'Flags candidate somatic sites using a user-specified flag (--filterFlag)  when the position '
                                                ' is found either in the intervals of a BED file or another VCF file. '
                                                ' This tool is generic and compatible with any VCF file. '
                                                ' The user inputs a VCF file (--fin) and the output is another VCF file. '
                                                ' If an input ARFF (--arff) and output ARFF (--arffout) file are also specified, presence (1) '
                                                ' or absence (0) is reported in the output ARFF file as a binary feature.'"", prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--fin', help='Input VCF', required=True)
parser.add_argument('--fout', help='Output VCF', required=True)
parser.add_argument('--arff', help='Optional input features file. If this is included we will add to the arff file the difference in average mapping quality for all valid somatic sites', required=False)
parser.add_argument('--arffout', help='Optional output features file. If this is included we will add to the arff file the difference in average mapping quality for all valid somatic sites', required=False)
parser.add_argument('--vcf', help='External VCF to check for overlap, e.g dbSNP, panel of normals, Cosmic', required=False)
parser.add_argument('--bed', help='BED file with intervals to check for overlag, e.g low mappability regions', required=False)
parser.add_argument('--filterFlag', help='Flag sites using this flag', required=True, default=None)
options = parser.parse_args()
        
from datetime import datetime
startTime = datetime.now()

if options.arff != None and options.arffout == None:
    parser.error("You need to specify both an input and an output ARFF file")
    
elif options.arff == None and options.arffout != None:
    parser.error("You need to specify both an input and an output ARFF file")
    
if options.vcf == None and options.bed == None :
    parser.error('Need to specify a VCF or a BED file to flag overlapping entries ')

if options.vcf != None and options.bed != None :
    parser.error('Need to specify either a vcf or a bed file to flag overlapping entries ')
    

if options.vcf != None:
    print "\nRunning",__file__, "against", options.vcf
    sites = filt_util.getOverlappingSites(options.fin, options.vcf, options.filterFlag)

elif options.bed != None:
    print "\nRunning",__file__, "against", options.bed
    sites = filt_util.getSitesOverlappingInterval(options.fin, options.bed, options.filterFlag)
    
    
filt_util.flagSites(list(sites.index), options.fout,
                     options.fin, options.filterFlag)

if options.vcf != None:
    filt_util.addVcfHeader('##FILTER=<ID=%s,Description="VCF entry overlaps with %s">\n' %(options.filterFlag,options.vcf) , 38
                        , options.fout)
                        
elif options.bed != None:
    filt_util.addVcfHeader('##FILTER=<ID=%s,Description="VCF entry overlaps with %s">\n' %(options.filterFlag,options.bed) , 38
                        , options.fout)

print "Feature computation completed."
if options.arff != None and options.arffout != None:
    filt_util.appendArffIfPresent(sites, options.arff, options.filterFlag, options.arffout)

print "Time elapsed:", datetime.now() - startTime