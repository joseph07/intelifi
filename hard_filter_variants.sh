#!/bin/bash

usage() {
echo ""
echo "This is a convenience script for automating variant filtering using some of the tools provided by InteliFI."
echo "It takes as inputs the VCF and ARFF file produced by multiSNV and does all of the following:"
echo ""
echo "1) Annotate variants using VEP"
echo "2) Run flagGermlineRiskSites.py"
echo "3) Run flagPossiblyGermline.py"
echo "4) Run flagAsymmetricMapping.py"
echo "5) Run flagNearbyIndels.py"
echo "6) Run flagOverlappingEntries.py with a) dbSNP version 137 excluding sites 129 (in datasets/) b) PoN (in datasets/)  "
echo ""
echo "The PoN was created at the CRUK-CI by running MuTect in artefact detection mode using 112 normal samples."
echo "The output is a VCF and ARFF file with the extension ${fin}_hard_filters.vcf and ${fin}_hard_filters.arff."
echo "To obtain a VCF with high-confidence variants simply retain all VCF entries flagged as PASS."
echo ""
echo "Usage:"
echo ""
echo "$0 --bams=\"bam1.bam bam2.bam\" --ref=/PATH/TO/fasta.fa --vep=/PATH/TO/";
echo "variant_effect_predictor.pl --cache=/PATH/TO/HOMOSAPIENS --fin=/PATH/TO/input.vcf --arff=/PATH/TO/arff.vcf [-h |--help] "

echo ""
echo "Options:"
echo ""
echo "--bams   Input BAM files in the same order that was used to run multiSNV i.e \"bam1.bam bam2.bam bam3.bam\""
echo "--ref    Fasta reference file";
echo "--vep    Path to variant_effect_predictor.pl";
echo "--cache  VEP cache directory";
#echo "--PoN    Panel of normals";
#echo "--dbSNP  dbSNP" ;
#echo "--validated = Optional: List of experimentally validated SNVs in a tab-delimited file of CHROM \t POSITION ";
echo "--fin    VCF produced by multiSNV"
echo "--arff   ARFF produced by multiSNV using --features option"
echo "--help   Produce this help message"
echo "";
exit
}

if [ $# -eq 0 ]; then
usage
exit 1;
fi

while [ "$#" -gt 0 ]; do
case "$1" in
--help) usage; exit 1;;
--bams=*) BAMS="${1#*=}"; shift 1;;
--ref=*) REF="${1#*=}"; shift 1;;
--vep=*) VEP="${1#*=}"; shift 1;;
--cache=*) CACHE="${1#*=}"; shift 1;;
--fin=*) multisnvVCF="${1#*=}"; shift 1;;
--arff=*) multisnvARFF="${1#*=}"; shift 1;;
--validated=*) VALIDATED="${1#*=}"; shift 1;;
--bams|--ref|--vep|--validated|--cache|--fin|--arff) echo "$1 requires an argument" >&2; exit 1;;
-*) echo "unknown option: $1" >&2; exit 1;;
*) die "unrecognized argument: $1";;
esac
done

if [ -z "$BAMS" ] ; then
echo "--bams is required"
exit 1;
fi

if [ -z $REF ] ; then
echo "--ref is required"
exit 1;
fi

if  [ -z $multisnvVCF ] ; then
echo "--fin is required"
exit 1;
fi

if  [ -z $multisnvARFF ] ; then
echo "--arff is required."
exit 1;
fi

if  [ -z $VEP ] ; then
echo "--vep is required. "
exit 1;
fi

if  [ -z ${CACHE} ] ; then
echo "--cache is required"
exit 1;
fi

#if  [ -z $PON ] ; then
#echo "No panel of normals was specified"
##exit 1;
#fi
#
#if  [ -z $DBSNP ] ; then
#echo "No dbSNP was specified"
#exit 1;
#fi



##DO NOT CHANGE ANYTHING AFTER THIS POINT, OTHER THAN FILTER NAMES!!!

REPO="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DBSNP=${REPO}/datasets/dbsnp_137_b37_excluding_sites_after129.vcf
echo "Using", ${DBSNP}

PON=${REPO}/datasets/CRUK_CI_PoN.vcf
echo "Using", ${PON}

echo $REPO
new_name="hard_filters"
finalVCF=${multisnvVCF%.vcf}_${new_name}.vcf
finalARFF=${multisnvARFF%.arff}_${new_name}.arff
INDEL_THRESH=0.05
#2. Annotate Using VEP
outputVCF=${multisnvVCF%.vcf}_temp_vep.vcf

perl ${VEP} \
--offline \
--vcf \
--dir_cache=${CACHE} \
--buffer_size 100 \
--no_progress \
--force_overwrite \
-i ${multisnvVCF} \
-o ${outputVCF} \
--species homo_sapiens \
--assembly GRCh37 \
--fasta ${REF} \
--no_stats \
--everything

outputARFF=${multisnvARFF}
#3 Run probabilistic filter to flag known germline sites
filter1="knownMaf"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter1}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_temp_${filter1}.arff

python ${REPO}/flagGermlineRiskSites.py --bams ${BAMS} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF}

#Run probabilistic filter to flag sites that are possibly germline
filter2="germlineRisk"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter2}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter2}.arff

python ${REPO}/flagPossiblyGermline.py --bams ${BAMS} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF}

#Flag sites with asymmetric mapping qualities in ref/variant reads
filter3="asymmetricMapping"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter3}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter3}.arff

python ${REPO}/flagAsymmetricMapping.py --bams ${BAMS} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF}

#Flag sites where more than a percentage of aligned reads spans an indel
filter6="NEARBY_INDELS"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter6}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter6}.arff

python ${REPO}/flagNearbyIndels.py  --bams $BAMS --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF} --arffout ${outputARFF} --threshold $INDEL_THRESH

#Flag sites overlapping dbSNP
filter4="dbSNP"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter4}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter4}.arff

python ${REPO}/flagOverlappingEntries.py --vcf ${DBSNP} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF} --filterFlag ${filter4}


if [ -z ${PON+x} ]

then :

else echo "Flagging variants that overlap with ${PON}"


filter5="PoN"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter5}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter5}.arff

python ${REPO}/flagOverlappingEntries.py --vcf ${PON} --fin ${inputVCF} --fout ${outputVCF} --arff ${inputARFF}  --arffout ${outputARFF} --filterFlag ${filter5}

fi

if [ -z ${VALIDATED+x} ]

then :

else echo "Flagging validated SNVs with ${VALIDATED}"

filter6="Validated"
inputVCF=${outputVCF}
outputVCF=${inputVCF%.vcf}_${filter6}.vcf
inputARFF=${outputARFF}
outputARFF=${inputARFF%.arff}_${filter6}.arff

python ${REPO}/flagOverlappingEntries.py --vcf ${VALIDATED} --fin ${inputVCF} --fout ${outputVCF} --arf\
f ${inputARFF}  --arffout ${outputARFF} --filterFlag ${filter6}
fi

mv ${outputVCF} ${finalVCF}
mv ${outputARFF} ${finalARFF}

rm ${multisnvVCF%.vcf}_temp*




