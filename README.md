# README #

InteliFi is a software package that provides tools for variant filtering. It provides downstream filters that compute features and meta-features of candidate somatic sites. It also provides scripts to hard-filter variants or to train an AdaBoost classifier to filter variants using labeled candidate somatic sites.  All tools are compatible with multiSNV VCF files and some are compatible with any VCF file. 

## Requirements

InteliFi requires python (tested for version 2.7.x) and the following packages: 

*  PyVCF (available through pipy (https://pypi.python.org/pypi/PyVCF))
*  scipy (tested on $0.17.0$)
*  numpy ($1.10.4$)
*  pandas ($0.16.2$)
*  pysam (available through pipy (https://pypi.python.org/pypi/pysam))

Some InteliFi tools require the Variant Effect Predictor (VEP) and the homo sapiens cache database to be installed. VEP requires the Perl modules Archive::Extract and DBD::mysql. 
InteliFi requires input VCF files to be sorted and header lines are only allowed in the beginning of the file.
 
##Installation##
Install the required python modules using pip and download InteliFi:
 

```
#!bash

pip install pysam PyVCF numpy pandas scipy
git clone https://bitbucket.org/joseph07/intelifi.git 

```
## Overview ##
The software package currently consists of the following command-line tools:

*   flagAsymmetricMapping.py
*   flagGermlineRiskSites.py
*   flagNearbyIndels.py
*   flagOverlappingEntries.py
*   flagPossiblyGermline.py
*   train.py
*   predict.py
*   train_default_model.py
*   hard_filter_variants.sh
*   intel_filter_variants.sh



### Running tool ###
For usage information check the --help option for each tool. 

Briefly, the shell script hard_filter_variants.sh flags multiSNV VCF files using various hard filters. 
The python tool train_default_model.py trains an AdaBoost classifier to discriminate between real and artefactual SNVs using the labeled dataset multisnv_MB99_all_flags.arff in the directory default/dataset/train. This file which consists of annotated somatic variants that are known to be real or artefacts.

The shell script intel_filter_variants.sh takes in a multiSNV VCF and ARFF file (the ARFF file can be obtained by running multiSNV with the --features option). This script automates feature computation and variant prediction using the previously trained AdaBoost model. It outputs a new filtered VCF file, consisting of all variants that have been predicted to be real, with the extension _AdaBoost.vcf

### Datasets ###

InteliFi includes a number of datasets that are useful for variant filtering, such as publicly available low-mappability regions and a panel of normals that was created at the Cancer Research UK Cambridge Institute. 

The BED file in datasets/LCR_Li2014.bed was published by Heng Li and downloaded from https://raw.githubusercontent.com/lh3/varcmp/master/scripts/LCR-hs37d5.bed.gz

The Blacklist Regions in datasets/DacBL.bed was created by Anshul Kundaje (ENCODE Project Consortium and downloaded from http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDacMapabilityConsensusExcludable.bed.gz

The BED file in datasets/DukeBL.bed are the Duke Excluded Regions tracks created by Terry Furey, Debbie Winter  and Stefan Graf and was downloaded from https://www.encodeproject.org/files/ENCFF001THR/@@download/ENCFF001THR.bed.gz

The panel of normals CRUK_CI_PoN.vcf was created at the CRUK-CI by running MuTect in the tumour-only mode using 112 normal samples from the oesophageal ICGC project. The oesophageal ICGC project was funded through a programme and infrastructure grant to Rebecca Fitzgerald as part of the OCCAMS collaboration.