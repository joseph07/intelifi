# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 00:55:46 2016

@author: joseph07
"""

from sklearn.grid_search import GridSearchCV
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
import time
from sklearn.externals import joblib
from sklearn.metrics import fbeta_score
import math
import pandas as pd
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from matplotlib.pyplot import cm 
import os
from matplotlib import pyplot as plt
import sys
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import make_scorer,confusion_matrix
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
from sklearn import decomposition
import arff_reader as arff
reload(arff)
import matplotlib.colors as mcolors
from sklearn.externals import joblib
import argparse

parser = argparse.ArgumentParser(description="" 'Train and save an AdaBoost model using a labeled .arff file.' 
                                                ' Performs grid search to tune hyperparameters' 
                                                ' where each setting is evaluated using 10-fold cross validation'
                                                ' of the F_beta statistic (beta=4)'
                                                ' The user can specify which input features to use (or a default set is used).'
                                                ' The user needs to specify the column name that corresponds to the SNV label (true/false)' "", prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--arff', help='Input arff', required=True)
parser.add_argument('--features', help='Arff columns to use as features',  nargs='+', type=str, required=False, default =  ['NORMAL_CONT_COUNTS', 'NORMAL_DEPTH',
 'MEAN_DEPTH',
 'FRACTION_FAILING_QC',
 'HOMOPOLYMER_RUN',
 'STRAND_BIAS',
 'VARIANT_QUALITY',
 'START_MEDIAN_DIST',
 'START_MEDIAN_ABSOLUTE_DEVIATION',
 'END_MEDIAN_DIST',
 'END_MEDIAN_ABSOLUTE_DEVIATION',
 'N_VARIANT_FREQ',
 'T1_VARIANT_FREQ',
 'POSSIBLY_GERMLINE',
 'ASYMMETRIC_MAPPING',
 'dbSNP',
 'PoN',
 'DacBL',
 'DukeBL',
 'LCR',
 'NEARBY_INDELS'])
 
parser.add_argument('--label', help='Column name corresponding to label', required=True)
parser.add_argument('--model', help='Output model', required=True)
options = parser.parse_args()

##Only T1_VARIANT_FREQ can be used - if multiple samples are present T1 represents the max variant freq across all samples

label = options.label
trainArff = options.arff
input_features = options.features
saved_model = options.model

dataset = arff.loadDataset(trainArff)
dataset  = arff.onlyMaxVariantFreq(dataset) 
    
dataset = dataset[input_features + [label]]
dataset= arff.removeMissing(dataset)
y_train = dataset[label]

X_train = dataset[input_features]

customScorer = make_scorer(fbeta_score,beta=4)
weights = y_train * (float(sum(y_train==0))/len(y_train)) + (1-y_train)*(1-float(sum(y_train==0))/len(y_train))

tuned_parameters = [{'n_estimators': [250,500,750, 1000,1250], 
                     'base_estimator__max_depth': [1,2],
                     'learning_rate': [0.25, 0.5,1]}]


clf = GridSearchCV(estimator=AdaBoostClassifier(DecisionTreeClassifier(), algorithm='SAMME'), param_grid=tuned_parameters,  scoring= customScorer, cv=10, fit_params={'sample_weight': weights.values}, verbose=20)              
clf.fit(X_train, y_train)

clf = clf.best_estimator_
    
model = {'model':clf, 'features':input_features}
joblib.dump(model, saved_model, compress=1)


#params, means, scores = zip(*clf.grid_scores_)
#params = [(param['base_estimator__max_depth'],param['learning_rate'], param['n_estimators']) for param in params]
#params = np.asarray(params)
#means=np.array(means)
#
######Plots######
#mpl.rcParams['legend.fontsize'] = 11
#mpl.rcParams['legend.markerscale'] = 0 
#
#fig, ax = plt.subplots(1,2,figsize=(10,4),sharey=True)
#for i,depth in enumerate([1,2]):
#    ind = np.array(np.where(params[:,0]==depth))
#    for eta in np.unique(params[:,1]):
#        ind2=np.array(np.where(params[:,1]==eta))
#        ind3 = np.intersect1d(ind,ind2)
#        n_estimators = params[ind3,2]
#        ax[i].plot(n_estimators.transpose(), means[ind3] ,'-o', label=r'$\eta=%s$' %eta) 
#
#    ax[i].set_ylabel(r'Cross validation score ($F_\beta$ score)', fontsize=14)
#    ax[i].set_xlabel('Number of estimators', fontsize=14)
#    ax[i].tick_params(axis="both", labelsize=16)
#    ax[i].grid(True)
#    ax[i].legend(loc='best')
#    ax[i].set_title('Maximum depth = %s' %depth)

#fig.savefig(output_figure, bbox_inches = 'tight')

##Plot feature importances
#importances = clf.feature_importances_
#indices = np.argsort(importances)
#names = X_train.columns
#
## Plot the feature importances
#plt.figure()
#plt.title("Feature importances", fontsize=16)
#plt.bar(range(X_train.shape[1]), importances[indices],
#       color="r", align="center")
#plt.xticks(range(X_train.shape[1]), names[indices], rotation=90, fontsize=14)
#plt.tick_params(axis='both', labelsize=14)
#plt.xlim([-1, X_train.shape[1]])
#
#plt.savefig(feature_importance_fig, bbox_inches = 'tight')

