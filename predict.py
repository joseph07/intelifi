# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 20:00:20 2016

@author: joseph07
"""

import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
import vcf
import argparse
import os
import filter_utils as filt_util
reload(filt_util)
from sklearn.ensemble import AdaBoostClassifier
import arff_reader as arff
reload(arff)
from sklearn.externals import joblib

parser = argparse.ArgumentParser(description="" 'Script to predict false variants using a pre-trained model. Outputs a filtered VCF file with variants predicted as True' "", prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--fin', help='Input vcf', required=True)
parser.add_argument('--arff', help='Input arff', required=True)
parser.add_argument('--model', help='Specify which pre-trained model to use', required=False,  default = os.path.dirname(os.path.realpath(__file__))+ "/default/model/default_model.pkl")
parser.add_argument('--fout', help='Output vcf', required=True)
options = parser.parse_args()

model =joblib.load(options.model)
print "\nPredicting false SNVs using", options.model
clf=model['model']
features = model['features']    

input_arff = options.arff
input_vcf = options.fin
output_vcf = options.fout

X = arff.loadDataset(input_arff)
X  = arff.onlyMaxVariantFreq(X) 

X = X[features]
X = arff.removeMissing(X)
pred =clf.predict(X)
passSites = list(X.index[pred==1])
filt_util.getVCFsubset(input_vcf, output_vcf, passSites)

filt_util.addVcfHeader('##Retain variants predicted as true using model=%s\n' %(options.model), 5,options.fout)

print "Prediction has been completed."