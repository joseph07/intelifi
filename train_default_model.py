# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 00:55:46 2016

@author: joseph07
"""

from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
import time
from sklearn.externals import joblib
from sklearn.metrics import fbeta_score
import math
import pandas as pd
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from matplotlib.pyplot import cm 
import os
from matplotlib import pyplot as plt
import sys
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import make_scorer,confusion_matrix
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
from sklearn import decomposition
import arff_reader as arff
reload(arff)
import matplotlib.colors as mcolors
from sklearn.externals import joblib
import argparse
parser = argparse.ArgumentParser(description="" ' Train and save an AdaBoost classifier using the training dataset provided. '
                                                ' Hyperparameters are set to the optimal settings found by grid search using 10 fold cross-validation, i.e:'
                                                ' Max_depth =2 , learning_rate = 0.25, base_estimators = 500 for this dataset \n'
                                                ' By default it uses all 21 features in the training arff file. To change the default feature set you should specify the column names of features in the arff file using --features feature1 feature2 feature3\n '
                                                ' By default, the output model is saved in /default/model/default_model.pkl.'
                                                ' as a dictionary of the `model` and the features used. This model can then be used for prediction with predict.py ' "", prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--features', help='List of features to use',  nargs='+', type=str, required=False, default =  ['NORMAL_CONT_COUNTS', 'NORMAL_DEPTH',
 'MEAN_DEPTH',
 'FRACTION_FAILING_QC',
 'HOMOPOLYMER_RUN',
 'STRAND_BIAS',
 'VARIANT_QUALITY',
 'START_MEDIAN_DIST',
 'START_MEDIAN_ABSOLUTE_DEVIATION',
 'END_MEDIAN_DIST',
 'END_MEDIAN_ABSOLUTE_DEVIATION',
 'N_VARIANT_FREQ',
 'T1_VARIANT_FREQ',
 'POSSIBLY_GERMLINE',
 'ASYMMETRIC_MAPPING',
 'dbSNP',
 'PoN',
 'DacBL',
 'DukeBL',
 'LCR',
 'NEARBY_INDELS'])

parser.add_argument('--model', help='Output model', required=False, default = os.path.dirname(os.path.realpath(__file__))+ "/default/model/default_model.pkl")
options = parser.parse_args()
##Only T1_VARIANT_FREQ can be used - if multiple samples are present T1 represents the max variant freq across all samples
label = 'Validated'

# features used are:",  options.features
trainArff= os.path.dirname(os.path.realpath(__file__)) + '/default/dataset/train/multisnv_MB99_all_flags.arff'
print "Training the default model using", trainArff
input_features = options.features
saved_model = options.model

print "Training dataset is ", trainArff

dataset = arff.loadDataset(trainArff)
dataset  = arff.onlyMaxVariantFreq(dataset) 
dataset = dataset[input_features + [label]]
dataset= arff.removeMissing(dataset)
y_train = dataset[label]
X_train = dataset[input_features]

weights = y_train * (float(sum(y_train==0))/len(y_train)) + (1-y_train)*(1-float(sum(y_train==0))/len(y_train))

clf=AdaBoostClassifier(DecisionTreeClassifier(max_depth=2), n_estimators=500, algorithm='SAMME', learning_rate=0.25)
clf.fit(X_train, y_train, sample_weight=weights.values)

model = {'model':clf, 'features':input_features}
joblib.dump(model, saved_model, compress=1)


