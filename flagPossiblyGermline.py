# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 18:00:46 2016

@author: joseph07
""" 
import vcf
import argparse
import numpy as np
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
import pandas as pd
import math
import filter_utils as filt_util
import VEPvcf as vep
import GermlineRisk as germlineFilter
import multisnv_vcf_utils as utils
from datetime import datetime

parser = argparse.ArgumentParser(description="" ' Flags candidate somatic sites that are germline variants as "POSSIBLY_GERMLINE" when the posterior probability '
                                                ' of being heterozgyous germline is greater than the posterior probability of '
                                                ' the reported somatic event given the user-specified minor allele frequency reported in the VEP annotated VCF. '
                                                ' The user specifies a minimum mapping and base quality to filter reads and the likelihood is computed using reads that '
                                                ' are retained. The default mapping quality (15) is lower than the default mapping quality of multiSNV (30) as a large number of events that appear to be '
                                                ' somatic are actually germline SNPs where one of the two alleles ends up under represented because of stringent mapping quality criteria.'
                                                ' Candidate somatic sites where the normal sample is heterozygous or with more than one mutation'
                                                ' per locus are ignored. '
                                                ' The tool is only compatible with multiSNV produced VCF files as it requires an allelic composition set to be reported.' 
                                                ' Usage: Input a multiSNV VCF file (--fin) and the BAM files in the same order that was used to run multiSNV. '
                                                ' The output is another VCF file. If an input ARFF (--arff) and output ARFF (--arffout) file are specified the log ratio of the posterior probability of being germline versus being somatic '
                                                ' is added to the output ARFF file. Any candidate somatic sites that are not known variants are appended by a "?" instead. '"", prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--fin', help='Input vcf', required=True)
parser.add_argument('--arff', help='Optional features file. If this is included we will add to the arff file the log ratio of P(Germline|Data) to P(Somatic|Data)', required=False)
parser.add_argument('--arffout', help='Optional features file. If this is included we will add to the arff file the log ratio of P(Germline|Data) to P(Somatic|Data)', required=False)
parser.add_argument('--bams', help='List of Bam files',  nargs='+', type=str, required=True)
parser.add_argument('--min_mapping_quality', help = "Minimum required mapping quality", required = False, default = 15)
parser.add_argument( '--min_base_quality', help = "Minimum required base quality", required = False, default = 20)
parser.add_argument( '--mut', help = "Mutation rate", required = False, default = 0.0001)
parser.add_argument( '--maf', help = "Minor Allele Frequency", required = False, default = 0.001)
parser.add_argument( '--fout', help='Output vcf', required=True)
options = parser.parse_args()
        
startTime = datetime.now()

if options.arff != None and options.arffout == None:
    sys.exit("You need to specify both an input and an output ARFF file")
    
elif options.arff == None and options.arffout != None:
    sys.exit("You need to specify both an input and an output ARFF file")

print "\nRunning",__file__
zetaT = utils.getParameterFromVCFheader("Hyperparameters of the Dirichlet prior on variant allele frequency in tumour", options.fin)
zetaN = utils.getParameterFromVCFheader("Hyperparameters of the Dirichlet prior on variant allele frequency in normal", options.fin)

#TODO: Parse these from vcf file
filterFlag = 'POSSIBLY_GERMLINE'

parameters = {'mapq':options.min_mapping_quality, 'baseq':options.min_base_quality, 'zetaN': zetaN, 'zetaT' : zetaT, 'passFilters' : ['LOW_QUAL', 'PASS',''], 'mut' : options.mut}

possiblyGermlineSites = germlineFilter.computeGermlineSomaticPosteriorLogRatio(options.fin, options.bams, parameters , options.maf, filterFlag)

#This does not require vep annotation: It will also include the vast majority of the knownGermline sites anyway (around 11/12!)
filt_util.flagSites(list(possiblyGermlineSites[possiblyGermlineSites[filterFlag] > 0].index), options.fout, 
                     options.fin, filterFlag)
filt_util.addVcfHeader('##FILTER=<ID=%s,Description="The posterior probability of being germline is higher than the posterior probability of being somatic when the mapping quality threshold is set to %s, the minor allele frequency is assumed to be %s and mutation rate is %s">\n' %(filterFlag, parameters['mapq'], options.maf, options.mut)
                        , 38,options.fout)

print "Feature computation completed."
if options.arff != None and options.arffout != None:
    #print "\nGenerating new ARFF file:", options.arffout
    filt_util.appendArff(possiblyGermlineSites, options.arff, filterFlag, options.arffout)
    
print "Time elapsed:", datetime.now() - startTime