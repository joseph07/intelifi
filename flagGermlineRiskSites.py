# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 13:12:26 2015

@author: joseph07
"""
import argparse
import numpy as np
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src/")
import filter_utils as filt_util
import VEPvcf as vep
import GermlineRisk as germlineFilter
from datetime import datetime
import multisnv_vcf_utils as utils


parser = argparse.ArgumentParser( description=""' Flags candidate somatic sites that are known germline variants as "KNOWN_GERMLINE" when the posterior probability '
                                                ' of being heterozgyous germline is greater than the posterior probability of '
                                                ' the reported somatic event given the minor allele frequency reported in the VEP annotated VCF.'
                                                ' The user specifies a minimum mapping and base quality to filter reads and the likelihood is computed using reads that'
                                                ' are retained. The default mapping quality (15) is lower than the default mapping quality of multiSNV (30) as a large number of events that appear to be '
                                                ' somatic are actually germline SNPs where one of the two alleles ends up under represented because of stringent mapping quality criteria.'
                                                ' Candidate somatic sites where the normal sample is heterozygous or with more than one mutation'
                                                ' per locus are ignored. '
                                                ' The tool is only compatible with multiSNV produced VCF files as it requires an allelic composition set to be reported.' 
                                                ' Usage: Input a multiSNV VCF file (--fin) and the BAM files in the same order that was used to run multiSNV. '
                                                ' The output is another VCF file. If an input ARFF (--arff) and output ARFF (--arffout) file are specified the log ratio of the posterior probability of being germline versus being somatic '
                                                ' is added to the output ARFF file. Any candidate somatic sites that are not known variants are appended by a "?" instead.'"",  prog='PROG',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--fin', help='Input VEP annotated vcf', required=True)
parser.add_argument( '--fout', help='Output vcf', required=True)
parser.add_argument('--bams', help='List of Bam files',  nargs='+', type=str, required=True)
parser.add_argument('--min_mapping_quality', help = "Minimum required mapping quality", required = False, default = 15)
parser.add_argument('--arff', help='Optional features file.', required=False)
parser.add_argument('--arffout', help='Optional output features file. If this is included we will add to the arff file the log ratio of P(Germline|Data) to P(Somatic|Data)', required=False)
parser.add_argument('--min_base_quality', help = "Minimum required base quality", required = False, default = 20)
parser.add_argument( '--mut', help = "Mutation rate", required = False, default = 0.0000001)
options = parser.parse_args()


        
startTime = datetime.now()

if options.arff != None and options.arffout == None:
    sys.exit("You need to specify both an input and an output ARFF file")
    
elif options.arff == None and options.arffout != None:
    sys.exit("You need to specify both an input and an output ARFF file")

print "\nRunning",__file__
zetaT = utils.getParameterFromVCFheader("Hyperparameters of the Dirichlet prior on variant allele frequency in tumour", options.fin)
zetaN = utils.getParameterFromVCFheader("Hyperparameters of the Dirichlet prior on variant allele frequency in normal", options.fin)

#TODO: Parse these from vcf file
parameters = {'mapq':options.min_mapping_quality, 'baseq':options.min_base_quality, 'zetaN': 200, 'zetaT' : 8, 'passFilters' : ['LOW_QUAL', 'PASS',''], 'mut' : options.mut}

filterFlag = 'KNOWN_GERMLINE'

knownGermlineSites = germlineFilter.computeGermlineRiskSites(options.fin, options.bams, parameters, filterFlag)
filt_util.flagSites(list(knownGermlineSites[knownGermlineSites[filterFlag] > 0].index), options.fout, 
                     options.fin, filterFlag )
filt_util.addVcfHeader('##FILTER=<ID=%s,Description="Site is a known germline and the posterior probability of being germline is higher than the posterior probability of being somatic (using minor allele frequencies from 1000 Genomes after VEP annotation). Using settings' %filterFlag + str(parameters) + '">\n' 
                        , 38, options.fout)

print "Feature computation completed."
if options.arff != None and options.arffout != None:
    filt_util.appendArff(knownGermlineSites, options.arff, filterFlag, options.arffout)
    
print "Time elapsed:", datetime.now() - startTime
